<?php

use Illuminate\Database\Seeder;
use App\Customer;
use App\Event;

class EventsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customer = Customer::where('name', 'Marco')->first();

        Event::create([
            'customer_id' => $customer->id,
            'type' => 'food',
            'location' => 'Rudi-Dutschke-Straße 18, 10969 Berlin',
            'lat' => 52.5065386,
            'lng' => 13.3919387,
        ]);
    }
}
