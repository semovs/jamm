<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    private $seeders = [
        'users' => UsersTableSeeder::class,
        'categories' => CategoriesTableSeeder::class,
        'customers' => CustomersTableSeeder::class,
        'customer_category' => '',
        'events' => EventsTableSeeder::class,
        'event_replies' => EventRepliesTableSeeder::class,
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Model::unguard();

        foreach (array_reverse($this->seeders, true) as $table => $seeder) {
            DB::table($table)->delete();
        }

        foreach ($this->seeders as $seeder) {
            if ($seeder) {
                $this->call($seeder);
            }
        }

        Model::reguard();
    }
}
