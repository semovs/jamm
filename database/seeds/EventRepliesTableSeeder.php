<?php

use Illuminate\Database\Seeder;
use App\Customer;
use App\Event;
use App\EventReply;

class EventRepliesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $replies = [
            'Julia' => false,
            'Mao' => true,
            'Andrejs' => true,
        ];

        $event = Event::all()->first();

        foreach ($replies as $customerName => $willAttend) {
            $customer = Customer::where('name', $customerName)->first();

            EventReply::create([
                'event_id' => $event->id,
                'customer_id' => $customer->id,
                'will_attend' => $willAttend
            ]);
        }
    }
}
