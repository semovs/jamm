<?php

use Illuminate\Database\Seeder;

class CustomersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $customers = [
            [
                'name' => 'Marco',
                'categories' => [
                    'Bike',
                    'Vegan',
                    'Kinky'
                ]
            ],
            [
                'name' => 'Andrejs',
                'categories' => [
                    'Ping Pong',
                    'Bike',
                    'Grill'
                ]
            ],
            [
                'name' => 'Mao',
                'categories' => [
                    'Kinky',
                    'Bike',
                    'Dance'
                ]
            ],
            [
                'name' => 'Julia',
                'categories' => [
                    'Dance',
                    'Vegan',
                    'Romanians'
                ]
            ]
        ];

        foreach ($customers as $customer) {
            $entity = \App\Customer::create([
                'name' => $customer['name'],
                'token' => crc32(serialize($customer)),
            ]);

            foreach ($customer['categories'] as $categoryName) {
                $category = \App\Category::where('name', $categoryName)->first();
                if ($category) {
                    $entity->categories()->attach($category->id);
                }
            }
        }
    }
}
