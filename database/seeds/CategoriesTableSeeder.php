<?php

use Illuminate\Database\Seeder;
use App\Category;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'food' => ['Eat', 'Vegan', 'Drink', 'Grill'],
            'activity' => ['Bike', 'Run', 'Dance', 'Ping Pong'],
            'other' => ['Romanians', 'Kinky', 'Other']
        ];

        foreach ($data as $group => $categories) {
            foreach ($categories as $category) {
                Category::create([
                    'name' => $category,
                    'slug' => str_slug($category),
                    'group' => $group
                ]);
            }
        }
    }
}
