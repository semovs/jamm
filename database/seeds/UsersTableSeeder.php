<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $predefinedUsers = [
            [
                'id' => 1,
                'name' => 'Admin',
                'email' => 'admin@jamm.dev',
                'password' => bcrypt('jamm')
            ]
        ];

        foreach ($predefinedUsers as $data) {
            DB::table('users')->insert($data);
        }
    }
}
