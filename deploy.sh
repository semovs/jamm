#!/bin/bash

cd /var/www/jamm-app

git checkout develop
git pull origin develop

composer install

php artisan migrate --force

