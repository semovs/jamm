<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('/user', 'UserController@index');

Route::get('/customer/{token}/events', 'EventController@index')->where('token', '[a-z0-9]+');
Route::post('/customer/{token}/event', 'EventController@create')->where('token', '[a-z0-9]+');
Route::post('/customer/{token}/event/{id}/reply', 'EventReplyController@create')
    ->where('token', '[a-z0-9]+')->where('id', '\d+');

Route::get('/category', 'CategoryController@index');
Route::get('/category/{slug}/customers', 'CustomerController@index')->where('token', '[a-z0-9\-]+');;

//Route::get('/user', function (Request $request) {
//    return $request->user();
//})->middleware('auth:api');
