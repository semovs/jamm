<?php

namespace App\Providers;

use App\Services\CategoryService;
use App\Services\CustomerService;
use App\Services\EventReplyService;
use App\Services\EventService;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->singleton(CustomerService::class);
        $this->app->singleton(CategoryService::class);
        $this->app->singleton(EventService::class);
        $this->app->singleton(EventReplyService::class);
    }
}
