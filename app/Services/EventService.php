<?php

namespace App\Services;
use App\Customer;
use App\Event;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class EventService
 * @package App\Services
 */
class EventService extends AbstractService
{
    /** @var CustomerService */
    protected $customer;

    public function __construct(CustomerService $customer)
    {
        $this->customer = $customer;
    }

    public function findAll()
    {
        return Event::all();
    }

    public function findById($id)
    {
        return Event::find($id);
    }

    public function findByCustomer(Customer $customer)
    {
        return Event::where('customer_id', $customer->id)->get();
    }

    /**
     * @param Customer $customer
     * @param ParameterBag $json
     * @return array
     */
    public function createEvent(Customer $customer, ParameterBag $json)
    {
        /** @var Event $event */
        $event = Event::create([
            'customer_id' => $customer->id,
            'type' => $json->get('type'),
            'location' => $json->get('location'),
            'lat' => $json->get('lat'),
            'lng' => $json->get('lng'),
        ]);

        $notifications = $this->sendNotifications($event);

        return [
            'success' => true,
            'event_id' => $event->id,
            'notifications' => $notifications
        ];
    }

    public function sendNotifications(Event $event)
    {
        $customer = $event->customer;
        $result = $this->customer->findByCustomerCategories($customer);

        return count($result);
    }
}
