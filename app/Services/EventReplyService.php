<?php

namespace App\Services;

use App\Customer;
use App\Event;
use App\EventReply;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Class EventReplyService
 * @package App\Services
 */
class EventReplyService extends AbstractService
{
    /** @var CustomerService */
    protected $customer;

    public function __construct(CustomerService $customer)
    {
        $this->customer = $customer;
    }

    public function findByCustomer(Customer $customer)
    {
        return EventReply::where('customer_id', $customer->id)->get();
    }

    public function findByCustomerAndEvent(Customer $customer, Event $event)
    {
        return EventReply::where([['customer_id', $customer->id], ['event_id', $event->id]])->get();
    }

    /**
     * @param Customer $customer
     * @param Event $event
     * @param ParameterBag $json
     * @return array
     */
    public function createEventReply(Customer $customer, Event $event, ParameterBag $json)
    {
        /** @var EventReply $eventReply */
        $eventReply = EventReply::create([
            'event_id' => $event->id,
            'customer_id' => $customer->id,
            'will_attend' => $json->get('will_attend'),
        ]);

        return [
            'success' => true,
            'event_reply_id' => $eventReply->id
        ];
    }
}
