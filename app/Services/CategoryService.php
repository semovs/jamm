<?php

namespace App\Services;
use App\Category;

/**
 * Class CategoryService
 * @package App\Services
 */
class CategoryService extends AbstractService
{
    public function __construct()
    {
    }

    public function findAll()
    {
        return Category::all();
    }

    public function findBySlug($slug)
    {
        return Category::where('slug', $slug)->first();
    }
}
