<?php

namespace App\Services;

use App\Category;
use App\Customer;

/**
 * Class CustomerService
 * @package App\Services
 */
class CustomerService extends AbstractService
{
    /** @var CategoryService */
    protected $category;

    public function __construct(CategoryService $category)
    {
        $this->category = $category;
    }

    /**
     * @return \Illuminate\Database\Eloquent\Collection|static[]
     */
    public function findAll()
    {
        return Customer::all();
    }

    /**
     * @param Category $category
     * @return mixed
     */
    public function findByCategory(Category $category)
    {
        $customers = Customer::whereHas('categories', function ($query) use ($category) {
            $query->where('category_id', $category->id);
        })->get();

        return $customers;
    }

    /**
     * @param Customer $customer
     * @return mixed
     */
    public function findByCustomerCategories(Customer $customer)
    {
        $customers = Customer::whereHas('categories', function ($query) use ($customer) {
            $query->whereIn('category_id', $customer->categories);
        })->get();

        return $customers;
    }

    /**
     * @param string $token
     * @return mixed
     */
    public function findByToken($token)
    {
        return Customer::where('token', $token)->first();
    }
}
