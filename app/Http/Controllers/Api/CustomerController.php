<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use App\Services\CustomerService;

class CustomerController extends Controller
{
    /** @var CustomerService */
    protected $customer;

    /** @var CategoryService */
    protected $category;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(CustomerService $customer, CategoryService $category)
    {
//        $this->middleware('auth');

        $this->customer = $customer;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug)
    {
        $category = $this->category->findBySlug($slug);
        if (!$category) {
            return $this->errorResponse('Category not found');
        }

        return $this->customer->findByCategory($category);
    }
}
