<?php

namespace App\Http\Controllers\Api;

use App\Event;
use App\Http\Controllers\Controller;
use App\Services\CustomerService;
use App\Services\EventReplyService;
use App\Services\EventService;
use Illuminate\Support\Facades\Request;

class EventReplyController extends Controller
{
    /** @var EventService */
    protected $event;

    /** @var EventReplyService */
    protected $eventReply;

    /** @var CustomerService */
    protected $customer;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EventService $event, EventReplyService $eventReply, CustomerService $customer)
    {
//        $this->middleware('auth');

        $this->event = $event;
        $this->eventReply = $eventReply;
        $this->customer = $customer;
    }

    public function create($token, $id)
    {
        $json = Request::json();
        $customer = $this->customer->findByToken($token);
        if (!$customer) {
            return $this->errorResponse('Customer not found');
        }

        $event = $this->event->findById($id);
        if (!$event) {
            return $this->errorResponse('Event not found');
        }

        if ($this->eventReply->findByCustomerAndEvent($customer, $event)) {
            return $this->errorResponse('Customer already replied to the event invitation');
        }

        /*
         * @TODO Currently it's possible to reply to any event if ID is known
         * should validate invited customer shares common categories with event owner
         * or better we should log event invites and check if the customer was invited.
         */
        $response = $this->eventReply->createEventReply($customer, $event, $json);

        return $response;
    }
}
