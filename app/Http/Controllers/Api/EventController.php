<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\CustomerService;
use App\Services\EventService;
use Illuminate\Support\Facades\Request;

class EventController extends Controller
{
    /** @var EventService */
    protected $event;

    /** @var CustomerService */
    protected $customer;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(EventService $event, CustomerService $customer)
    {
//        $this->middleware('auth');

        $this->event = $event;
        $this->customer = $customer;
    }

    /**
     * @param string $token
     * @return mixed
     */
    public function index($token)
    {
        $customer = $this->customer->findByToken($token);
        if (!$customer) {
            return $this->errorResponse('Customer not found');
        }

        return $this->event->findByCustomer($customer);
    }

    public function create($token)
    {
        $json = Request::json();
        $customer = $this->customer->findByToken($token);
        if (!$customer) {
            return $this->errorResponse('Customer not found');
        }

        $response = $this->event->createEvent($customer, $json);

        return $response;
    }

    public function reply($token)
    {
        $json = Request::json();
        $customer = $this->customer->findByToken($token);
        if (!$customer) {
            return $this->errorResponse('Customer not found');
        }

        $response = $this->event->createReply($customer, $json);

        return $response;
    }
}
