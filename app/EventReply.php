<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class EventReply extends Model
{
    protected $fillable = [
        'event_id', 'customer_id', 'will_attend'
    ];
}
